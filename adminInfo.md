# Info for (student) IT admins

## Contact

In principle UCO (uco@desy.de) is the first point of contact for any IT related problem.
Right now, our PCs are registered under the theory network admins (they give us the ip addresses), but are then part of our own gwat resources, see below.
There's a mailing list to contact them (theory-computing@desy.de) in order to register new PCs.

## Setting up new linux desktop machines

All desktops need first to be registered by a network admin in order to get access to the network.
Send an email to (theory-computing@desy.de) with the computer name that is written on a sticker in front (something like `pcx40203`) and its MAC address, and tell them to register them under our group-name "gw" (i.e. it should be registered as something like `gwpcx40203`).
You can find the MAC address using a live system (e.g. a Ubuntu live USB stick) or similar.
Sometimes the BIOS also gives this information.

Once the PC is registered in the network, send an email to (linux@desy.de) and ask them to enable the network installation of a "Green" Ubuntu desktop (see [https://confluence.desy.de/display/linux/DESY+Linux+Desktop] for other options).
Once they have enabled it, plug the PC to the network and start it.
If it doesn't show you a boot menu, you might have to press F10 or F12 during the boot process to get there.
In the boot menu choose "boot from network (UEFI)".
You should then be lead to a menu which let's you choose to install Ubuntu on the PC automatically.
Just run that, drink a coffee and wait for it to finish (1hour+).
Congratulations, you have a working Linux installed.

## DESY registry (user management)

The central DESY tool to administrate users is the registry [https://registry.desy.de/registry/].
We have our own namespace and group for GWAT that we admin ourselves (currently Rafael and Gregor are namespace admins). 
Only people that have been given access to the gwat resource can log into our PCs. 
For some info on the registry see [https://www.desy.de/registry/index.html], or the registry_manual.pdf in this repo.
To become a namespace admin you will have to take part in an official introduction, contact UCO.

## Mailing list

Mailing lists are administrated via [https://lists.desy.de/sympa/].
The first time you log in (with your DESY email address) you have to create a password.
We admin our own [gwat@desy.de] mailing list.
