# List of GWAT PCs

| PC name    | MAC address       | user         | room      | OS           | RAM  |
| gwpcx40203 | 00:30:59:20:5c:91 | Gregor       | 1a EG-117 | Ubuntu 22.04 | 64GB |
| gwpcx40209 | 00:30:59:20:5d:2d | Christoph    | 1a EG-117 | Ubuntu 20.04 | 64GB |
| gwpcx40218 | 00:30:59:20:5c:a0 | Mateja       | 1a EG-121 | Ubuntu 22.04 | 16GB |
| gwpcx40207 | 00:30:59:20:5b:ee | Massimiliano | 1a EG-121 | Ubuntu 20.04 | 16GB |
| gwpcx40204 | 00:30:59:20:5c:8a | Loris        | 1a EG-121 | Ubuntu 20.04 | 16GB |
| gwpcx40210 | 00:30:59:20:5d:eb | Zixin        | 1a EG-121 | Ubuntu 20.04 | 16GB |
