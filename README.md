# IT and the PAX cluster @ DESY

## Requirements

* Terminal (google will teach you how to use a terminal and a text editor in there!)
* DESY computer account (see below)
* SSH (Secure SHell) client: a programm to connect to a different computer via a network. Most Linux and OSX systems have an ssh client installed by default.

## Getting help

For problems with any IT service in Hamburg, contact the UCO under uco@desy.de. For Zeuthen it's uco-zn@desy.de.
Right now, Gregor is the student IT admin for our group.
He can help you as well.

## Computer account

In order to get a DESY computer account you have to fill in the form that the group computer admins (current Rafael and Gregor) can print for you via the registry.

(The following is probably deprecated since the automated print-out from the registry will pre-fill the sheet.)
On the first page use "Z-THAT" for the DESY group, and choose yes for "Limited account for max. 3 years". I suggest to check the boxes "UNIX: zsh (default)" and "Windows: Zeuthen". Fill in the rest of page 1. Ask an older colleague if you are lost. Don't forget to sign on the bottom of the page.
On the second page just fill in the "Primary group" with "ZN-THAT". Leave the rest of page 2 blank and let it sign by Rafael under "Group Leader" and by Cristina Guerrero (bld 2a/room 403) under "Secretariat".
Then scan and send it to UCO-Zeuthen uco-zn@desy.de.

## SSH portals into DESY

There are two portals that allow you to login within your terminal into the DESY network. Formally it will open for you a terminal on a DESY login node.
At Hamburg there is bastion. Use this command in your terminal to login:
```
ssh <your_desy_username>@bastion.desy.de
```
where you have to replace `<your_desy_username>` (don't type the "<" and ">") by your DESY username that you also use for login into your mail account and similar.
At Zeuthen there is the login node called warp or transfer:
```
ssh <your_desy_username>@warp.zeuthen.desy.de
```

On bastion and warp you are not supposed to run your computations (they are only portals into the DESY network), but instead you can login into your desktop that sits under your desk (given you have one) or the cluster. Do so by e.g.
```bash
ssh gwpcx40203
```
where the PC name is written on a sticker in front of your machine (+ add a "gw" in front of it).
You can also find the name of your machine in the file `PCList.md` in this repository.

## Using the Mathematica or Maple license server from home

I use the program called sshuttle (https://github.com/sshuttle/sshuttle) to route my internet traffic through bastion or warp. I.e., being on my laptop/PC at home I type:
```
sshuttle --dns -r <your_desy_username>@131.169.5.82 -x 131.169.5.82 0/0
```
131.169.5.82 is (one of) the IP address of bastion, you can look it up by typing
```
ping -c1 bastion.desy.de
```
Just leave this terminal running somewhere. All your internet traffic is now routed via DESY, and Mathematica and Maple will be able to connect to the license server (zitlic03.desy.de).


## Disk space in the afs

`afs` is the central file system at DESY.
You have a home directory (~) on bastion and/or on warp.
To get there, simply type `cd` or `cd ~` when logged into bastion or warp.
(The tilde always stands for your home directory).
You don't have that much space in your home directory. If you need more, contact your IT group leader, UCO or UCO-ZN.
For more information about the DESY filesystem, check https://it.desy.de/services/storage_services/afs/index_eng.html.

You can use the `scp` command to exchange files between your PC and the DESY filesystem. It works very similarly as the usual `cp` command. For example you can copy the mathematica installation file from the DESY filesystem with
```
scp <your_desy_username>@bastion.desy.de:/afs/desy.de/products/source/mathematica/12.1/Mathematica_12.1.1_LINUX.sh .
```
This will copy this file into your current directory on your computer. (For more software, check https://it.desy.de/services/soft/downloads/index_eng.html)
It also works the other way around, e.g. copy a file from your current directory into your home directory on bastion:
```
scp yourFile.txt <your_desy_username>@bastion.desy.de:~/yourFile.txt
```

On the Zeuthen side (i.e. for warp/PAX) you can also use the lustre disk space for big files. Just make a folder for yourself under `/lustre/fs24/group/that/`.

## Disk space on your desktop

If you have a desktop PC at DESY, you can use all the disk space on that machine for yourself.
On each desktop (yes, you can log into someone else's desktop as well) you have a different home (~) directory, even though some folders are mapped to some of your folders in the afs.
To find out, what is local and what is from the afs, type
```bash
ls -lah ~
```
Currently the directories `afs`, `Documents`, `private`, `public`, and `www` are not local folders, but are mapped folders from the afs.
Hence, don't store big data in them (again, you don't have a lot of space on the afs).

## Software guides

### Git

Git is a very powerful version control system. It takes some initial effort to learn, but once you handle it, it's great to keep track of your files and collaborate with others.
Two good tutorials is suggest are:
* https://www.atlassian.com/git/tutorials/what-is-version-control
* https://www.tutorialspoint.com/git/index.htm

There are several platforms that offer (free) hosting of your git repository:
* gitlab: unlimited free private and public repository
* BitBucket: (unlimited?) free private and public repository
* GitHub: free public repositories

DESY also has its own gitlab server https://gitlab.desy.de/. To confuse you a little bit more: Note that gitlab refers to two different things here: It can either mean the gitlab software, which is used on servers to host gitlab repositories, or it can mean the "official" gitlab hosting service which hosts repositories for you (which uses the gitlab software itself). 

## The PAX cluster @ DESY Zeuthen

There is a general guide for the cluster under https://dv-zeuthen.desy.de/services/parallel_computing/.

### Basics

The cluster is divided into four subclusters, pax9 to pax12, called "Sandybridge", "Haswell", "Broadwell", and "Rome" respectively.
Each of these subclusters itself is divided into nodes, either 16 (pax9 and pax11) or 32 (pax10 and pax12). Each node then has a certain number of CPU cores and memory. See the above webpage for the exact stats. To run computations you have to reserve time on one or more nodes. (This cluster is setup such that you cannot only book a few cores, so you always get at least one full node.)
You can either interactively book a node, i.e. you get some timeframe reserved when you are allowed to login (with ssh, see below) into the node and run computations directly there. Or you can submit a batch file, which is a script that will be automatically run once there is a free slot in any of the nodes. For both ways you will have to wait for a free slot. It can be fast, but if you are unlucky it can also take a lot of time. This also depends on how many nodes, for how much time and on which subcluster you try to book time. Below I describe these steps in more detail.

### Login

In order to use PAX you have to log into the pax login node which is called pax9-00. It's essentially a special node on pax9. (Even if you want to use, say, pax11, you have to log into pax9-00 to book time).
You first have to log into warp, see above, and then you can simply type:
```
ssh pax9-00
```
and you will be logged into pax9-00.

### SLURM

The software that takes care of the time bookings is called slurm. Pax9-00  you will be able to start (interactive&batch) jobs for PAX.

Arguments for interactive & batch (usual slurm):
* `-p <sandybridge/haswell/broadwell/rome>`: partition to use
* `-n`: number of cores (divide this number by the number of cores of your chosen partition (rounded up) and that's how many nodes you will be given).
* `-J`: job name
* `-t dd-hh:mm:ss`: time, defaults to 30 minutes, max is 2 days.

Using more than one node is not so trivial because of sharing memory among the nodes. Read about (Open)MPI or ask Gregor in case you are interested in Mathematica code that can do it.

### Modules

To use certain programs like Mathematica you'll have to load the corresponding module, e.g.
```
module load mathematica/12.1
```
You can check all available modules with `module avail` (e.g. different versions of mathematica). Whenever you log into another pax node you have to reload the modules. You also have to load them when you run a batch script, see below.

Some important modules are:
* mathematica/xx.y for Mathematica version xx.y
* gnux/x.y.z for the GNU compiler set version x.y.z

### Interactively
Reserve time:
```
salloc <slurm arguments>
```
where `<slurm arguments>` has to contain the arguments above (and more if you want).
e.g.
```
salloc -p sandybridge -n1 -t 00-00:30:00
```
for one node for half an hour on sandybridge.
Then check which node you have been assigned, once there's an output:
```
squeue -u <your_desy_username>
```
The output will be something like:
```
[pax9-00] ~ % squeue -u grekae
            138751 sandybrid      zsh   grekae  R       0:11      1 pax9-07
```
You can then ssh into that node with
```
ssh pax9-07
```
and use it.
If you are done you and still have leftover time you can cancel the job by
```
scancel 138751
```
for the above example. With the `squeue` you can always check what time you (and others) have reserved, and how big the waiting queue is.

#### With a batch script

It works very similarily as the interactive allocation, except that you put all your code that you want to run into a file and call it for example "run.sh". The following example runs a mathematica (.m) file:
```
#!/bin/bash -l

#SBATCH -p rome
#SBATCH -n 32
#SBATCH -t 2-00:00:00
#SBATCH -J replIBP

#load modules
module load mathematica/12.1 gnu9/9.3.0

#actual code I want to run
export FERMATPATH=/afs/ifh.de/group/that/work-gregor/fermat/fer64
cd /afs/ifh.de/group/that/work-gregor/grfeynrules/
wolfram -script run.m
```

Now you can submit that file to the SLURM queue with
```
sbatch run.sh
```
Whenever you get assign a time slot the code in that file will be run. You can always check with `squeue` about the status of your computation. This command will also produce a file called `slurm-<number>.out` where all the terminal output of your file will be put.

### Installing software on PAX

If your favourite program is not installed on PAX you have to install it yourself in your own directory. Need help? Either contact uco-zn.desy.de or ask your collaborators if they have already installed it.


## GWAT webpage

Our webpage can be found under [https://astroparticle-physics.desy.de/research/theory/gwat/index_eng.html].
You can edit it by going to [https://astroparticle-physics.desy.de/research/theory/gwat/manage/].
You might be able to login with your usual DESY login.
If not, contact weboffice@desy.de to activate your account for editing the webpage.
They can also help you if there's any problem during the editing process.

Please add your publications and talks to the subpage LHCtoLISA.
The easiest way is to copy one entry and then just replace what needs to be changed.

## Billing (Kostenstelle)

We have at the moment the following Kostenstellen to pay for various things:
* GWAT: 904 1000
* THAT: 904 0000
* LHCToLisa: 904 1106
